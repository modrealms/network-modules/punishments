package net.modrealms.punishments.sponge.menu;

import com.codehusky.huskyui.states.Page;
import com.codehusky.huskyui.states.State;
import com.codehusky.huskyui.states.action.ActionType;
import com.codehusky.huskyui.states.action.ClickType;
import com.codehusky.huskyui.states.action.runnable.RunnableAction;
import com.codehusky.huskyui.states.element.ActionableElement;
import com.mongodb.gridfs.GridFSDBFile;
import net.modrealms.core.object.Menu;
import net.modrealms.core.object.Punishment;
import net.modrealms.core.util.PlayerProfiles;
import net.modrealms.punishments.sponge.Punishments;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class PunishmentsMenu extends Menu {

    public PunishmentsMenu(Player player) {
        super(player);
    }

    public CompletableFuture<Menu> prepare(UUID target) {
        return getPunishmentsForPlayer(target).thenApply(page -> {
            this.getContainer().addState(page);
            return this;
        });
    }

    public CompletableFuture<Menu> prepare(String type) {
        return getPunishments(type).thenApply(page -> {
            this.getContainer().addState(page);
            return this;
        });
    }

    private CompletableFuture<State> getPunishments(String type) {
        return CompletableFuture.supplyAsync(() -> {
            Page.PageBuilder builder = this.setup(
                    "punishments-main",
                    new InventoryDimension(9, 4),
                    Text.of(TextColors.DARK_GRAY, "Network ", StringUtils.capitalize(type.toLowerCase()) + "s"),
                    true
            );

            for (Punishment punishment : Punishments.getInstance().getCore().getDaoManager().getPunishmentDAO().getPunishmentList().stream().filter(p -> p.getType().equalsIgnoreCase(type)).collect(Collectors.toList())) {
                builder.addElement(new ActionableElement(getSkull(punishment), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    new PunishmentClick(punishment).run();
                })));
            }

            return builder.build("main");
        });
    }

    private CompletableFuture<State> getPunishmentsForPlayer(UUID target) {
        return CompletableFuture.supplyAsync(() -> {
            Page.PageBuilder builder = this.setup(
                    "punishments-main",
                    new InventoryDimension(9, 4),
                    Text.of(TextColors.DARK_GRAY, "Player Punishments"),
                    true
            );

            for (Punishment punishment : Punishments.getInstance().getCore().getDaoManager().getPunishmentDAO().getPunishmentList().stream().filter(p -> p.getTarget().equals(target)).collect(Collectors.toList())) {
                builder.addElement(new ActionableElement(getSkull(punishment), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    new PunishmentClick(punishment).run();
                })));
            }

            return builder.build("main");
        });
    }

    private ItemStack getSkull(Punishment punishment) {
        GameProfile profile = PlayerProfiles.getPlayerProfile(punishment.getTarget());
        return ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.SKULL_TYPE, SkullTypes.PLAYER)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, StringUtils.capitalize(punishment.getType().toLowerCase())))
                .add(Keys.REPRESENTED_PLAYER, profile)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, profile.getName().get(), "'s ", StringUtils.capitalize(punishment.isComment() ? "comment" : punishment.getType().toLowerCase())))
                .add(Keys.ITEM_LORE, getPunishmentInformation(punishment))
                .build();
    }

    private class PunishmentClick implements Runnable {

        private Punishment punishment;

        public PunishmentClick(Punishment punishment) {
            this.punishment = punishment;
        }

        @Override
        public void run() {
            List<Text> content = getPunishmentInformation(punishment);
            if (punishment.isActive()) {
                content.add(Text.builder("[").color(TextColors.GRAY).append(Text.of(TextColors.RED, "Pardon")).append(Text.of(TextColors.GRAY, "]"))
                        .onClick(TextActions.executeCallback(source -> {
                            if (source.hasPermission("modrealms.punishment.delete")) {
                                Punishments.getInstance().getPluginMessenger().pardon((Player) source, punishment);
                                source.sendMessage(Text.of(TextColors.GREEN, "You have requested for this player to be pardoned and it will be fulfilled now."));
                            }
                        })).build());
            } else {
                content.add(Text.builder("[").color(TextColors.GRAY).append(Text.of(TextColors.DARK_RED, "Delete")).append(Text.of(TextColors.GRAY, "]"))
                        .onClick(TextActions.executeCallback(source -> {
                            if (source.hasPermission("modrealms.punishment.delete")) {
                                Punishments.getInstance().getPluginMessenger().delete((Player) source, punishment);
                                source.sendMessage(Text.of(TextColors.GREEN, "You have requested for this punishment to be deleted and it will be fulfilled now."));
                            }
                        })).build());
            }
            PaginationList.builder().contents(content).padding(Text.of(TextStyles.STRIKETHROUGH, TextColors.GRAY, "-")).title(Text.of(TextColors.GREEN, PlayerProfiles.getPlayerProfile(punishment.getTarget()).getName().get(), "'s ", StringUtils.capitalize(punishment.isComment() ? "comment" : punishment.getType().toLowerCase()))).sendTo(getPlayer());
        }
    }

    private List<Text> getPunishmentInformation(Punishment punishment) {
        return new ArrayList<Text>() {{
            add(Text.of(TextColors.GOLD, "Player: ", TextColors.GRAY, PlayerProfiles.getPlayerProfile(punishment.getTarget()).getName().get()));
            if (punishment.getStaff() != null) {
                add(Text.of(TextColors.GOLD, "Staff: ", TextColors.GRAY, PlayerProfiles.getPlayerProfile(punishment.getStaff()).getName().get()));
            }
            add(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, punishment.getReason()));
            add(Text.of(TextColors.GOLD, "Date: ", TextColors.GOLD, punishment.getDate()));
            if (punishment.isTemporary()) {
                add(Text.of(TextColors.GOLD, "Expiry: ", getDateColor(punishment.getExpiry()), punishment.getExpiry()));
            }
            if (punishment.getActive() != null) {
                add(Text.of(TextColors.GOLD, "Active: ", (punishment.isActive() ? TextColors.GREEN : TextColors.RED), punishment.isActive()));
            }
        }};
    }

    private TextColor getDateColor(Date date) {
        TextColor color;
        int untilExpiry = (int) ChronoUnit.HOURS.between(date.toInstant(), new Date().toInstant());
        if (untilExpiry < 1) color = TextColors.GREEN;
        else if (untilExpiry <= 4) color = TextColors.YELLOW;
        else if (untilExpiry <= 72) color = TextColors.RED;
        else color = TextColors.DARK_RED;
        return color;
    }
}
