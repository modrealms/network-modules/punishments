package net.modrealms.punishments.sponge;

import lombok.Getter;
import lombok.Setter;
import net.modrealms.connectionbridge.sponge.ConnectionBridge;
import net.modrealms.core.Core;
import net.modrealms.core.object.Punishment;
import net.modrealms.punishments.sponge.command.PunishmentsCommand;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.game.state.GameAboutToStartServerEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import javax.inject.Inject;
import java.nio.file.Path;
import java.util.ArrayList;

@Plugin(id = "punishments", name = "MRPunishments", version = "1.0", description = "Sponge plugin which allows you to view and handle punishments in the network.", dependencies = {@Dependency(id = "connectionbridge")})
public class Punishments {
    @Inject
    @Getter
    @Setter
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    @Getter
    private PluginMessenger pluginMessenger;

    private static Punishments plugin;

    public static Punishments getInstance(){
        return plugin;
    }

    @Listener(order = Order.DEFAULT)
    public void onServerStartedEvent(GameStartedServerEvent event){
        plugin = this;

        this.getCore().getLogger().info("Punishments is starting...");

        this.pluginMessenger = new PluginMessenger();

        Sponge.getCommandManager().register(this, new PunishmentsCommand(), "punishments");
    }

    public Core getCore(){
        return Core.getInstance();
    }
}
