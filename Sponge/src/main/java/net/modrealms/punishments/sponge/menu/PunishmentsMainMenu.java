package net.modrealms.punishments.sponge.menu;

import com.codehusky.huskyui.states.Page;
import com.codehusky.huskyui.states.State;
import com.codehusky.huskyui.states.action.ActionType;
import com.codehusky.huskyui.states.action.ClickType;
import com.codehusky.huskyui.states.action.runnable.RunnableAction;
import com.codehusky.huskyui.states.element.ActionableElement;
import net.modrealms.core.object.Menu;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class PunishmentsMainMenu extends Menu {
    public PunishmentsMainMenu(Player player) {
        super(player);
        addState(main());
    }

    public State main(){
        Page.PageBuilder builder = this.setup(
                null,
                new InventoryDimension(9, 3),
                Text.of(TextColors.DARK_GRAY, "Select a punishment"),
                true
        );

        builder.putElement(10, new ActionableElement(ItemStack.builder()
                .itemType(ItemTypes.YELLOW_FLOWER)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Kicks"))
                .build(), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    player.sendMessage(Text.of(TextColors.YELLOW, "Loading Kicks..."));
                    new PunishmentsMenu(this.player).prepare("KICK").thenAccept(Menu::open);
                })));

        builder.putElement(12, new ActionableElement(ItemStack.builder()
                .itemType(ItemTypes.RED_FLOWER)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Bans"))
                .build(), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    player.sendMessage(Text.of(TextColors.YELLOW, "Loading Bans..."));
                    new PunishmentsMenu(this.player).prepare("BAN").thenAccept(Menu::open);
                })));

        builder.putElement(14, new ActionableElement(ItemStack.builder()
                .itemType(ItemTypes.JUKEBOX)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Mutes"))
                .build(), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    player.sendMessage(Text.of(TextColors.YELLOW, "Loading Mutes..."));
                    new PunishmentsMenu(this.player).prepare("MUTE").thenAccept(Menu::open);
                })));

        builder.putElement(16, new ActionableElement(ItemStack.builder()
                .itemType(ItemTypes.PAPER)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Warnings"))
                .build(), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> {
                    player.sendMessage(Text.of(TextColors.YELLOW, "Loading Warnings..."));
                    new PunishmentsMenu(this.player).prepare("WARNING").thenAccept(Menu::open);
        })));
        return builder.build("punishments-main");
    }
}
