package net.modrealms.punishments.sponge.command;

import net.modrealms.core.object.Menu;
import net.modrealms.punishments.sponge.menu.PunishmentsMainMenu;
import net.modrealms.punishments.sponge.menu.PunishmentsMenu;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class PunishmentsCommand implements CommandCallable {
    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(source instanceof Player){
            Player player = (Player) source;
            if(testPermission(source)){
                if(arguments.isEmpty()){
                    new PunishmentsMainMenu(player).open();
                    return CommandResult.success();
                } else {
                    String[] args = arguments.split(" ");
                    try {
                        GameProfile targetProfile = Sponge.getServer().getGameProfileManager().get(args[0]).get();
                        source.sendMessage(Text.of(TextColors.YELLOW, "Loading Punishments..."));
                        new PunishmentsMenu(player).prepare(targetProfile.getUniqueId()).thenAccept(Menu::open);
                    } catch (InterruptedException | ExecutionException e) {
                        source.sendMessage(Text.of(TextColors.RED, "This player does not exist!"));
                    }
                }
            }
        }
        return CommandResult.empty();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments, @Nullable Location<World> targetPosition) throws CommandException {
        return new ArrayList<String>(){{
            for (Player player : Sponge.getServer().getOnlinePlayers()){
                add(player.getName());
            }
        }};
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("modrealms.punishments.gui");
    }

    @Override
    public Optional<Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.GRAY, "Opens up the punishment menu."));
    }

    @Override
    public Optional<Text> getHelp(CommandSource source) {
        return Optional.empty();
    }

    @Override
    public Text getUsage(CommandSource source) {
        return null;
    }
}
