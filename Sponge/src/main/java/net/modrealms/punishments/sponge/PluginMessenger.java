package net.modrealms.punishments.sponge;

import com.google.common.collect.Maps;
import net.modrealms.core.object.Punishment;
import org.spongepowered.api.Platform;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.sound.SoundCategories;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.network.ChannelBinding;
import org.spongepowered.api.network.ChannelBuf;
import org.spongepowered.api.network.RawDataListener;
import org.spongepowered.api.network.RemoteConnection;

import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PluginMessenger {
    private ChannelBinding.RawDataChannel channel;
    private final Punishments punishments = Punishments.getInstance();

    public PluginMessenger(){
        this.channel = Sponge.getChannelRegistrar().getOrCreateRaw(punishments, "BungeeCord");
        this.channel.addListener(new ChannelListener());
    }

    private void checkChannel(){
        if(channel == null){
            throw new IllegalStateException("The message channel could not be found!");
        }
    }

    public void pardon(Player player, Punishment punishment){
        checkChannel();
        channel.sendTo(player, buf -> buf.writeUTF("pardon").writeUTF(punishment.getId().toString()));
    }

    public void delete(Player player, Punishment punishment){
        checkChannel();
        channel.sendTo(player, buf -> buf.writeUTF("delete").writeUTF(punishment.getId().toString()));
    }

    private class ChannelListener implements RawDataListener {
        ConcurrentMap<Predicate<ChannelBuf>, Consumer<ChannelBuf>> map = Maps.newConcurrentMap();

        @Override
        public void handlePayload(ChannelBuf data, RemoteConnection connection, Platform.Type side) {
//            if (data.readUTF().equalsIgnoreCase("refreshpunishments")) {
//                punishments.getCore().getDaoManager().getPunishmentDAO().loadPunishments();
//            }
        }
    }
}
