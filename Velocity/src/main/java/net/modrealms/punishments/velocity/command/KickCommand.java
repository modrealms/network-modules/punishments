package net.modrealms.punishments.velocity.command;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.command.SimpleCommand;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.util.UuidUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.modrealms.core.object.Punishment;
import net.modrealms.core.util.PlayerProfiles;
import net.modrealms.punishments.velocity.Punishments;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.*;

public class KickCommand extends Temporary implements SimpleCommand {
    @Override
    public void execute(Invocation invocation) {
        CommandSource source = invocation.source();
        String[] args = invocation.arguments();
        if(!hasPermission(invocation)){
            source.sendMessage(Component.text("You do not have permission to run this command!").color(NamedTextColor.RED));
            return;
        }

        if(args.length < 2){
            source.sendMessage(Component.text("You must specify a player and a reason for the kick!").color(NamedTextColor.RED));
            return;
        }

        Optional<String> uuidString = PlayerProfiles.getOnlineUuid(args[0]);
        if(uuidString.isPresent()){
            Punishment punishment = new Punishment(UuidUtils.fromUndashed(uuidString.get()), "KICK")
                    .setReason(getReason(args))
                    .setExpiry(getEndDate(args).orElse(null))
                    .setStaff(source instanceof Player ? ((Player) source).getUniqueId() : null);
            punishmentDAO.addPunishment(punishment);
            Punishments.getInstance().getPunishmentManager().handle(punishment);
        } else {
            source.sendMessage(Component.text("You've specified an invalid player!").color(NamedTextColor.RED));
        }
    }

    @Override
    public boolean hasPermission(Invocation invocation) {
        CommandSource source = invocation.source();
        String[] args = invocation.arguments();
        return source.hasPermission("modrealms.punishment.kick.base");
    }
}
