package net.modrealms.punishments.velocity.command;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.command.SimpleCommand;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.util.UuidUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.modrealms.core.object.Punishment;
import net.modrealms.core.util.PlayerProfiles;
import net.modrealms.punishments.velocity.Punishments;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.*;

public class MuteCommand extends Temporary implements SimpleCommand {
    @Override
    public void execute(Invocation invocation) {
        CommandSource source = invocation.source();
        String[] args = invocation.arguments();
        List<String> argsList = new ArrayList<>(Arrays.asList(args));
        if(!hasPermission(invocation)){
            source.sendMessage(Component.text("You do not have permission to run this command!").color(NamedTextColor.RED));
            return;
        }

        if(args.length < 2){
            source.sendMessage(Component.text("You must specify a player and a reason for the mute!").color(NamedTextColor.RED));
            return;
        }

        Optional<String> uuidString = PlayerProfiles.getOnlineUuid(args[0]);
        if(uuidString.isPresent()){
            Punishment punishment = new Punishment(UuidUtils.fromUndashed(uuidString.get()), "MUTE")
                    .setReason(getReason(args))
                    .setExpiry(getEndDate(args).orElse(null))
                    .setStaff(source instanceof Player ? ((Player) source).getUniqueId() : null)
                    .setActive(true);
            punishmentDAO.addPunishment(punishment);
            Punishments.getInstance().getPunishmentManager().handle(punishment);
        } else {
            source.sendMessage(Component.text("You've specified an invalid player!").color(NamedTextColor.RED));
        }
    }

    @Override
    public boolean hasPermission(Invocation invocation) {
        CommandSource source = invocation.source();
        String[] args = invocation.arguments();
        List<String> argsList = new ArrayList<>(Arrays.asList(args));
        if(argsList.contains("--time")){
            return source.hasPermission("modrealms.punishment.mute.temporary");
        } else {
            return source.hasPermission("modrealms.punishment.mute.permanent");
        }
    }
}
