package net.modrealms.punishments.velocity.event;

import com.google.common.io.ByteArrayDataInput;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.PluginMessageEvent;
import com.velocitypowered.api.proxy.ServerConnection;
import com.velocitypowered.api.proxy.messages.LegacyChannelIdentifier;
import com.velocitypowered.api.proxy.messages.MinecraftChannelIdentifier;
import net.modrealms.core.data.dao.PunishmentDAO;
import net.modrealms.core.object.Punishment;
import net.modrealms.punishments.velocity.Punishments;

import java.util.Optional;
import java.util.UUID;

public class VelocityPluginMessage {
    private static final LegacyChannelIdentifier LEGACY_BUNGEE_CHANNEL = new LegacyChannelIdentifier("BungeeCord");
    private static final MinecraftChannelIdentifier MODERN_BUNGEE_CHANNEL = MinecraftChannelIdentifier.create("bungeecord", "main");

    private final Punishments punishments = Punishments.getInstance();
    private final PunishmentDAO punishmentDAO = punishments.getCore().getDaoManager().getPunishmentDAO();

    @Subscribe
    public void onPluginMessage(PluginMessageEvent event){
        if (!event.getIdentifier().equals(LEGACY_BUNGEE_CHANNEL) && !event.getIdentifier().equals(MODERN_BUNGEE_CHANNEL)) return;
        event.setResult(PluginMessageEvent.ForwardResult.handled());
        if (!(event.getSource() instanceof ServerConnection)) return;
        ByteArrayDataInput in = event.dataAsDataStream();
        String subChannel = in.readUTF();

        if (subChannel.equalsIgnoreCase("refreshpunishments")){
            punishments.getCore().getDaoManager().getPunishmentDAO().loadPunishments();
        } else if(subChannel.equalsIgnoreCase("pardon")){
            String id = in.readUTF();
            Optional<Punishment> punishment = punishmentDAO.getPunishmentList().stream().filter(p -> p.getId().toString().equalsIgnoreCase(id)).findAny();
            punishment.ifPresent(p -> punishments.getPunishmentManager().pardon(p));
        } else if(subChannel.equalsIgnoreCase("delete")){
            String id = in.readUTF();
            Optional<Punishment> punishment = punishmentDAO.getPunishmentList().stream().filter(p -> p.getId().toString().equalsIgnoreCase(id)).findAny();
            punishment.ifPresent(p -> punishments.getPunishmentManager().delete(p));
        }
    }
}
