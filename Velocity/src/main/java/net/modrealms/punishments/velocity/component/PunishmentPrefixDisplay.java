package net.modrealms.punishments.velocity.component;

import lombok.Data;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;

@Data
public class PunishmentPrefixDisplay {
    private Component component;
    private NamedTextColor color;

    public PunishmentPrefixDisplay(NamedTextColor color){
        this.color = color;
        this.component = craftComponent();
    }

    private Component craftComponent(){
        TextComponent.Builder builder = Component.text();
        builder.append(Component.text("[").color(NamedTextColor.GRAY));
        builder.append(Component.text("!").color(color).decoration(TextDecoration.BOLD, false));
        builder.append(Component.text("]").color(NamedTextColor.GRAY));
        return builder.build();
    }
}
