package net.modrealms.punishments.velocity;

import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.messages.LegacyChannelIdentifier;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import net.modrealms.core.data.dao.PunishmentDAO;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class VelocityPluginMessenger {
    public void sendPunishmentRefresh() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF("refreshpunishments");
            for(RegisteredServer server : Punishments.getInstance().getProxy().getAllServers()){
                server.sendPluginMessage(new LegacyChannelIdentifier("BungeeCord"), stream.toByteArray());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
