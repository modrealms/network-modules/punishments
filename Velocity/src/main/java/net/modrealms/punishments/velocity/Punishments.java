package net.modrealms.punishments.velocity;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Dependency;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.PluginContainer;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import lombok.Getter;
import net.modrealms.chat.velocity.Chat;
import net.modrealms.connectionbridge.velocity.ConnectionBridge;
import net.modrealms.core.Core;
import net.modrealms.punishments.velocity.command.BanCommand;
import net.modrealms.punishments.velocity.command.KickCommand;
import net.modrealms.punishments.velocity.command.MuteCommand;
import net.modrealms.punishments.velocity.command.WarnCommand;
import net.modrealms.punishments.velocity.event.VelocityConnection;
import net.modrealms.punishments.velocity.event.VelocityPluginMessage;
import net.modrealms.punishments.velocity.manager.PunishmentManager;
import javax.inject.Inject;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Logger;

@Plugin(id = "punishments", name = "MRPunishments", version = "1.0", description = "Velocity plugin for handling punishments throughout the network", dependencies = {@Dependency(id = "connectionbridge"), @Dependency(id = "chat")})
public class Punishments {
    @Getter
    private final ProxyServer proxy;
    @Getter
    private final Logger logger;
    @Getter
    private final Path directory;
    @Getter
    private VelocityPluginMessenger pluginMessenger;
    @Getter
    private PunishmentManager punishmentManager;
    @Getter
    private ConnectionBridge bridgeInstance;

    private static Punishments plugin;

    @Inject
    public Punishments(ProxyServer proxy, Logger logger, @DataDirectory Path directory){
        plugin = this;

        this.proxy = proxy;
        this.logger = logger;
        this.directory = directory;
    }

    @Subscribe(order = PostOrder.NORMAL)
    public void onProxyInitialization(ProxyInitializeEvent event) {
        Optional<PluginContainer> bridgeContainer = this.proxy.getPluginManager().getPlugin("connectionbridge");
        if(bridgeContainer.isPresent()){
            this.bridgeInstance = (ConnectionBridge) bridgeContainer.get().getInstance().get();
            this.bridgeInstance.getPluginManager().register(this.proxy.getPluginManager().fromInstance(this).get());
            this.getLogger().info("Successfully hooked onto ConnectionBridge");
        } else {
            this.getLogger().severe("ConnectionBridge doesn't exist!");
        }

        this.pluginMessenger = new VelocityPluginMessenger();
        this.punishmentManager = new PunishmentManager();

        this.proxy.getCommandManager().register("warning", new WarnCommand(), "warn");
        this.proxy.getCommandManager().register("kick", new KickCommand());
        this.proxy.getCommandManager().register("ban", new BanCommand());
        this.proxy.getCommandManager().register("mute", new MuteCommand());

        this.proxy.getEventManager().register(this, new VelocityConnection());
        this.proxy.getEventManager().register(this, new VelocityPluginMessage());
    }

    public static Punishments getInstance(){
        return plugin;
    }

    public Core getCore(){
        return this.bridgeInstance.getCore();
    }
}
