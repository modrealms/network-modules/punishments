package net.modrealms.punishments.velocity.event;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.ServerPreConnectEvent;
import net.modrealms.core.object.Punishment;
import net.modrealms.punishments.velocity.Punishments;

import java.util.Optional;

public class VelocityConnection {

    @Subscribe(order = PostOrder.FIRST)
    public void onServerPreConnect(ServerPreConnectEvent event){
        Optional<Punishment> banOptional = Punishments.getInstance().getCore().getDaoManager().getPunishmentDAO().getBan(event.getPlayer().getUniqueId(), event.getPlayer().getRemoteAddress().getHostString());
        if (banOptional.isPresent()) {
            event.getPlayer().disconnect(Punishments.getInstance().getPunishmentManager().getPunishmentNotice(banOptional.get()));
            event.setResult(ServerPreConnectEvent.ServerResult.denied());
        }
    }
}
