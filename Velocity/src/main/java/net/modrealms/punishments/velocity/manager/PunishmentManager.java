package net.modrealms.punishments.velocity.manager;

import com.velocitypowered.api.proxy.Player;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.modrealms.core.data.dao.PunishmentDAO;
import net.modrealms.core.object.Punishment;
import net.modrealms.core.util.PlayerProfiles;
import net.modrealms.punishments.velocity.Punishments;
import net.modrealms.punishments.velocity.component.PunishmentPrefixDisplay;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.stream.Collectors;

public class PunishmentManager {

    private final Punishments punishments = Punishments.getInstance();
    private final PunishmentDAO punishmentDAO = punishments.getCore().getDaoManager().getPunishmentDAO();

    public void handle(Punishment punishment){
        Optional<Player> target = punishments.getProxy().getPlayer(punishment.getTarget());
        if(punishment.getType().equalsIgnoreCase("MUTE")){
            punishments.getCore().getDaoManager().getBasePlayerDAO().getPlayer(punishment.getTarget()).ifPresent(basePlayer -> {
                basePlayer.setChannel("MUTED");
                basePlayer.update();
                punishments.getBridgeInstance().getPluginManager().getChat().ifPresent(chat -> chat.updateAllMeta(punishment.getTarget()));
            });
        }
        target.ifPresent(player -> {
            punishments.getCore().getDaoManager().getPunishmentDAO().loadPunishments();
            if(!punishment.isComment()) player.sendMessage(getPunishmentNotice(punishment));
            if(punishment.getType().equals("BAN") || punishment.getType().equals("KICK")){
                target.get().disconnect(getPunishmentNotice(punishment));
            }
        });
        for(Player staff : punishments.getProxy().getAllPlayers().stream().filter(p -> p.hasPermission("modrealms.chat.staff")).collect(Collectors.toList())){
            staff.sendMessage(getStaffPunishmentNotice(punishment));
        }
        punishments.getPluginMessenger().sendPunishmentRefresh();
    }

    public void pardon(Punishment punishment){
        punishment.setActive(false);
        punishmentDAO.addPunishment(punishment); // used to update

        if(punishment.getType().equalsIgnoreCase("MUTE")){
            punishments.getCore().getDaoManager().getBasePlayerDAO().getPlayer(punishment.getTarget()).ifPresent(basePlayer -> {
                basePlayer.setChannel("GLOBAL");
                basePlayer.update();
                punishments.getBridgeInstance().getPluginManager().getChat().ifPresent(chat -> chat.updateAllMeta(punishment.getTarget()));
            });
        }

        Optional<Player> target = punishments.getProxy().getPlayer(punishment.getTarget());
        target.ifPresent(player -> player.sendMessage(getPunishmentRemoval(punishment)));
    }

    public void delete(Punishment punishment){
        punishmentDAO.deletePunishment(punishment);
    }

    public TextComponent getPunishmentNotice(Punishment punishment){
        PunishmentPrefixDisplay prefix = new PunishmentPrefixDisplay(NamedTextColor.DARK_RED);

        TextComponent.Builder builder = Component.text().append(prefix.getComponent())
                .append(Component.text(" You have received a ").color(NamedTextColor.DARK_RED))
                .append(Component.text(StringUtils.capitalize(punishment.getType().toLowerCase())).color(NamedTextColor.RED));


        Optional<String> username = PlayerProfiles.getOnlineName(punishment.getStaff().toString());
        if(punishment.getStaff() != null){
            builder.append(Component.text(" from ").color(NamedTextColor.DARK_RED))
                    .append(Component.text(username.orElse(punishment.getStaff().toString())).color(NamedTextColor.RED));
        }
        builder.append(Component.text("!").color(NamedTextColor.DARK_RED));

        if(!punishment.getReason().isEmpty()){
            builder.append(Component.text(" '" + punishment.getReason() + "'").color(NamedTextColor.GRAY));
        }

        if(punishment.isTemporary()){
            builder.append(Component.text("\nExpiration Date: ").color(NamedTextColor.GOLD))
                    .append(Component.text(punishment.getExpiry().toString()).color(NamedTextColor.GRAY));
        }

        return builder.build();
    }

    public TextComponent getStaffPunishmentNotice(Punishment punishment){
        PunishmentPrefixDisplay prefix = new PunishmentPrefixDisplay(NamedTextColor.DARK_RED);

        String target = PlayerProfiles.getOnlineName(punishment.getTarget().toString()).orElse(punishment.getStaff().toString());

        TextComponent.Builder builder = Component.text().append(prefix.getComponent())
                .append(Component.text(" " + target + " has received a ").color(NamedTextColor.DARK_RED))
                .append(Component.text(StringUtils.capitalize(punishment.getType().toLowerCase())).color(NamedTextColor.RED));

        if(punishment.getStaff() != null){
            builder.append(Component.text(" from ").color(NamedTextColor.DARK_RED))
                    .append(Component.text(PlayerProfiles.getOnlineName(punishment.getStaff().toString()).orElse(punishment.getStaff().toString())).color(NamedTextColor.RED));
        }
        builder.append(Component.text("!").color(NamedTextColor.DARK_RED));

        if(!punishment.getReason().isEmpty()){
            builder.append(Component.text(" '" + punishment.getReason() + "'").color(NamedTextColor.GRAY));
        }

        if(punishment.isTemporary()){
            builder.append(Component.text("\nExpiration Date: ").color(NamedTextColor.GOLD))
                    .append(Component.text(punishment.getExpiry().toString()).color(NamedTextColor.GRAY));
        }

        return builder.build();
    }

    public TextComponent getPunishmentRemoval(Punishment punishment){
        PunishmentPrefixDisplay prefix = new PunishmentPrefixDisplay(NamedTextColor.DARK_RED);

        TextComponent.Builder builder = Component.text().append(prefix.getComponent())
                .append(Component.text(" Your " + punishment.getType().toLowerCase() + " has been lifted!").color(NamedTextColor.GREEN));

        return builder.build();
    }

}
