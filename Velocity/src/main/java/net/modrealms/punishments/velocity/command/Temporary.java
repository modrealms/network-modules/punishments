package net.modrealms.punishments.velocity.command;

import net.modrealms.core.data.dao.PunishmentDAO;
import net.modrealms.punishments.velocity.Punishments;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Temporary {

    public final PunishmentDAO punishmentDAO = Punishments.getInstance().getCore().getDaoManager().getPunishmentDAO();

    public Optional<Date> getEndDate(String[] arguments){
        List<String> args = new ArrayList<>(Arrays.asList(arguments));
        if(args.contains("--time")){
            int param = args.indexOf("--time");
            String durationString = args.get(param + 1);
            final Calendar duration = new GregorianCalendar();
            if(durationString.endsWith("s")){
                duration.add(Calendar.SECOND, Integer.parseInt(durationString.replace("s", "")));
            } else if(durationString.endsWith("m")){
                duration.add(Calendar.MINUTE, Integer.parseInt(durationString.replace("m", "")));
            } else if(durationString.endsWith("h")){
                duration.add(Calendar.HOUR_OF_DAY, Integer.parseInt(durationString.replace("h", "")));
            } else if(durationString.endsWith("d")){
                duration.add(Calendar.DAY_OF_MONTH, Integer.parseInt(durationString.replace("d", "")));
            } else if(durationString.endsWith("w")){
                duration.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(durationString.replace("w", "")));
            } else if(durationString.endsWith("mo")){
                duration.add(Calendar.MONTH, Integer.parseInt(durationString.replace("mo", "")));
            } else if(durationString.endsWith("y")){
                duration.add(Calendar.YEAR, Integer.parseInt(durationString.replace("y", "")));
            } else {
                return Optional.empty();
            }
            return Optional.of(duration.getTime());
        } else {
            return Optional.empty();
        }
    }

    public String getReason(String[] arguments){
        List<String> args = new ArrayList<>(Arrays.asList(arguments));
        args.remove(0); // player argument
        if(args.contains("--time")) {
            int param = args.indexOf("--time");
            args.remove(param); // remove the parameter
            args.remove(param); // remove the argument after it
        }
        args.remove("--comment"); // remove comment parameter

        return String.join(" ", args);
    }
}
